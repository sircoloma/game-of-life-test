<?php
/**
 * Created by PhpStorm.
 * User: raul
 * Date: 10/07/18
 * Time: 22:28
 */

namespace GameOfLife\Test;

use GameOfLife\Cell;
use GameOfLife\Board;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    /** @test */
    public function oneBoardHasDimensions()
    {
        $board = new Board(10, 10);

        $heightBoard = $board->getHeightBoard();
        $widthBoard = $board->getWidthBoard();

        $this->assertEquals(10, $heightBoard);
        $this->assertEquals(10, $widthBoard);
    }

    /** @test */
    public function oneEmptyBoardHasNoneCell()
    {
        $board = new Board(10, 10);

        $livingCells = $board->getLivingCells();

        $this->assertEquals(0, count($livingCells));
    }

    /** @test */
    public function oneBoardHasOneCell()
    {
        $board = new Board(10, 10);

        $board->insertCell(new Cell(1, 1));
        $livingCells = $board->getLivingCells();

        $this->assertEquals(1, count($livingCells));
    }

    /** @test */
    public function oneBoardHasAListOfPotentialCells()
    {
        $cellOne = new Cell(1, 1);
        $cellTwo = new Cell(2, 1);
        $board = new Board(10, 10);

        $board->insertCell($cellOne);
        $board->insertCell($cellTwo);

        $realListOfPotentialCells = [
            new Cell(0, 2), new Cell(1, 2), new Cell(2, 2), new Cell(3, 2),
            new Cell(0, 1), new Cell(1, 1), new Cell(2, 1), new Cell(3, 1),
            new Cell(0, 0), new Cell(1, 0), new Cell(2, 0), new Cell(3, 0),
        ];

        $potentialCells = $board->getPotentialCells();

        $this->assertCount(4 * 3, $potentialCells);
        $this->assertContainAllOf($potentialCells, $realListOfPotentialCells);
    }

    public function assertContainAllOf($arraySubjects, $objects)
    {
        $containAllOfObjects = true;

        foreach ($arraySubjects as $subject) {
            if (!in_array($subject, $objects)) {
                $containAllOfObjects = false;
                break;
            }
        }

        $this->assertTrue($containAllOfObjects, 'The subject is not completely contained in the object');
    }
}
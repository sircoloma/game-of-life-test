<?php

namespace GameOfLife\Test;

use GameOfLife\Cell;
use GameOfLife\GameOfLife;
use GameOfLife\Board;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: raul
 * Date: 9/07/18
 * Time: 20:25
 */
class GameOfLifeTest extends TestCase
{
    /** @test */
    public function seDebeCrearElJuegoConNumeroDeGeneracionesMaximas()
    {
        $juegoDeLaVida = new GameOfLife(6);

        $numeroDeGeneraciones = $juegoDeLaVida->getNumberOfGenerations();

        $this->assertEquals(6, $numeroDeGeneraciones);
    }

    /** @test */
    public function elJuegoPuedeManejarGeneracionesCorrectas()
    {
        $entornoPruebaInicial = [
            new Cell(2, 2),
            new Cell(3, 2),
            new Cell(4, 2)
        ];
        $entornoDespuesDePrimeraGeneracion = [
            new Cell(3, 2),
            new Cell(3, 1),
            new Cell(3, 3)
        ];

        $tablero = new Board(10, 10);

        foreach ($entornoPruebaInicial as $celula) {
            $tablero->insertCell($celula);
        }

        $siguiente = $tablero->nextGeneration();
        $celulasVivas = $siguiente->getLivingCells();

        $this->assertCount(3, $celulasVivas);
        $this->assertContieneTodoDe($celulasVivas, $entornoDespuesDePrimeraGeneracion);

        $siguiente = $siguiente->nextGeneration();
        $celulasVivas = $siguiente->getLivingCells();


        $this->assertCount(3, $celulasVivas);
        $this->assertContieneTodoDe($celulasVivas, $entornoPruebaInicial);
    }

    public function assertContieneTodoDe($arraySujetos, $objetos)
    {
        $contieneTodosLosObjetos = true;

        foreach ($arraySujetos as $sujeto) {
            if (!in_array($sujeto, $objetos)) {
                $contieneTodosLosObjetos = false;
                break;
            }
        }

        $this->assertTrue($contieneTodosLosObjetos, 'El sujeto no está completamente contenido en el objeto');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: raul
 * Date: 10/07/18
 * Time: 21:06
 */

namespace JuegoDeLaVida\Test;

use GameOfLife\Cell;
use GameOfLife\Board;
use PHPUnit\Framework\TestCase;

class CellTest extends TestCase
{
    /** @var  Cell */
    private $cell;

    public function setUp()
    {
        $this->cell = new Cell(1, 1);
    }

    /** @test */
    public function oneCellDetectANeighbour()
    {
        $neighbourCell = new Cell(1, 2);

        $isANeighbour = $this->cell->isANeighbour($neighbourCell);

        $this->assertTrue($isANeighbour);
    }

    /** @test */
    public function oneCellDetectIfIsNotANeighbour()
    {
        $neighbourCell = new Cell(1, 4);

        $isANeighbour = $this->cell->isANeighbour($neighbourCell);

        $this->assertFalse($isANeighbour);
    }

    /** @test */
    public function oneCellCannotBeNeighbourHimself()
    {
        $isANeighbour = $this->cell->isANeighbour($this->cell);

        $this->assertFalse($isANeighbour);
    }

    /** @test */
    public function oneCellCanDetermineHowManyNeighborsHas()
    {
        $environment = [
            new Cell(1, 1), new Cell(1, 2), new Cell(2, 1)
        ];

        $numberOfNeighboursIn = $this->cell->numberOfNeighboursIn($environment);

        $this->assertEquals(2, $numberOfNeighboursIn);
    }

    /** @test */
    public function oneCellCanDetermineIfSurviveOrDie()
    {
        $environment = [
            new Cell(1, 1), new Cell(1, 2), new Cell(2, 1)
        ];

        $surviveIn = $this->cell->surviveIn($environment);

        $this->assertTrue($surviveIn);
    }

    /** @test */
    public function oneCellDieIfAreAlone()
    {
        $environment = [];

        $surviveIn = $this->cell->surviveIn($environment);

        $this->assertFalse($surviveIn);
    }

    /** @test */
    public function oneCellDieIfHasFourNeighbors()
    {
        $environment = [
            new Cell(1, 1), new Cell(1, 2), new Cell(2, 1),
            new Cell(2,2), new Cell(4,4), new Cell(0, 1),
        ];

        $surviveIn = $this->cell->surviveIn($environment);

        $this->assertFalse($surviveIn);
    }

    /**
     * @test
     */
    public function oneCellHasToBeInTheLimitsOfTheBoard()
    {
        $board = new Board(10, 10);

        $board->insertCell(new Cell(1, 11));
        $livingCells = $board->getLivingCells();

        $this->assertEquals(0, count($livingCells));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: raul
 * Date: 11/07/18
 * Time: 20:39
 */
include 'GameOfLife.php';
include 'Cell.php';
include 'Board.php';

use GameOfLife\GameOfLife;


echo "*** Welcome to the Game Of Life ***\n\n";
echo "How much will it measure in WIDTH? -> ";
$width = readline();

if (!is_numeric($width)) {
    echo "Introduce a valid number !!\n";
    exit();
}

echo "How much will it measure in HEIGHT? -> ";
$height = readline();

if (!is_numeric($height)) {
    echo "Introduce a valid number !!\n";
    exit();
}


echo "How many cells do you want create randomly? -> ";
$numberOfCells = readline();

if (!is_numeric($numberOfCells)) {
    echo "Introduce a valid number !!\n";
    exit();
}

while ($numberOfCells < 3) {
    echo "We need 3 or more cells!! ->";
    $numberOfCells = readline();
}

echo "And now, how many generations? -> ";
$generations = readline();

if (!is_numeric($generations)) {
    echo "Introduce a valid number !!\n";
    exit();
}

$gameOfLife = new GameOfLife($generations);
$gameOfLife->initGameAndBoard($numberOfCells, $height, $width);
$gameOfLife->generatingGame();

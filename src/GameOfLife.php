<?php

namespace GameOfLife;

/**
 * Created by PhpStorm.
 * User: raul
 * Date: 9/07/18
 * Time: 20:25
 */
class GameOfLife
{
    const INFINITE_GENERATIONS = -1;

    /** @var  int $numberOfGenerations */
    private $numberOfGenerations;

    /** @var  Board */
    private $initialBoard;

    /**
     * GameOfLife constructor.
     * @param int $numberOfGenerations
     */
    public function __construct($numberOfGenerations = -1)
    {
        $this->numberOfGenerations = $numberOfGenerations;
    }

    /**
     * @return int
     */
    public function getNumberOfGenerations()
    {
        return $this->numberOfGenerations;
    }

    /**
     *
     */
    public function generatingGame()
    {
        $next = $this->initialBoard;
        $this->printNumberOfGeneration('Initial');
        $this->printBoard($next);

        $numberOfGeneration = $this->getNumberOfGenerations();

        if ($numberOfGeneration == self::INFINITE_GENERATIONS) {
            $generation = 1;
            while (true) {
                $next = $next->nextGeneration();
                $this->printNumberOfGeneration($generation);
                $this->printBoard($next);
                $generation++;
            }
        }

        for ($generation = 1; $generation <= $numberOfGeneration; $generation++) {
            $next = $next->nextGeneration();
            $this->printNumberOfGeneration($generation);
            $this->printBoard($next);
        }
    }

    /**
     * @param int $numberOfCells
     * @param int $heightBoard
     * @param int $widthBoard
     */
    public function initGameAndBoard($numberOfCells, $heightBoard, $widthBoard)
    {
        $initialEnvironment = [];
        $initialBoard = new Board($heightBoard, $widthBoard);

        for ($countCells = 1; $countCells <= $numberOfCells; $countCells++) {
            $positionX = rand(1, $widthBoard);
            $positionY = rand(1, $widthBoard);

            $initialEnvironment[] = new Cell($positionX, $positionY);
        }

        /** @var Cell $cell */
        foreach ($initialEnvironment as $cell) {
            $initialBoard->insertCell($cell);
        }

        $this->initialBoard = $initialBoard;
    }

    /**
     * Print the generated board
     * @param Board $board
     */
    public function printBoard($board)
    {
        $livingCells = $board->getLivingCells();
        $foundCell = false;

        for ($xAxis = 1; $xAxis <= $board->getWidthBoard(); $xAxis++) {
            for ($yAxis = 1; $yAxis <= $board->getHeightBoard(); $yAxis++) {

                $provisionalCell = new Cell($xAxis, $yAxis);

                foreach ($livingCells as $livingCell) {
                    if ($provisionalCell == $livingCell) {
                        $foundCell = true;
                    }
                }
                if ($foundCell) {
                    echo " X ";
                } else {
                    echo " · ";
                }
                $foundCell = false;
            }
            echo "\n";
        }
        echo "\n\n";
        sleep(1);
    }

    /**
     * Print title of the number of generation
     * @param $paramOfGeneration
     */
    private function printNumberOfGeneration($paramOfGeneration)
    {
        echo "\n";
        echo "Generation ".$paramOfGeneration;
        echo "\n";
    }
}

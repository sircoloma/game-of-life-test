<?php
/**
 * Created by PhpStorm.
 * User: raul
 * Date: 9/07/18
 * Time: 21:12
 */

namespace GameOfLife;


class Board
{
    /** @var int */
    private $heightBoard;

    /** @var int */
    private $widthBoard;

    /** @var array */
    private $livingCells = [];

    /**
     * Board constructor.
     * @param int $height
     * @param int $width
     */
    public function __construct($height, $width)
    {
        $this->heightBoard = $height;
        $this->widthBoard = $width;
    }

    /**
     * @return int
     */
    public function getHeightBoard(): int
    {
        return $this->heightBoard;
    }

    /**
     * @param int $heightBoard
     */
    public function setHeightBoard(int $heightBoard)
    {
        $this->heightBoard = $heightBoard;
    }

    /**
     * @return int
     */
    public function getWidthBoard(): int
    {
        return $this->widthBoard;
    }

    /**
     * @param int $widthBoard
     */
    public function setWidthBoard(int $widthBoard)
    {
        $this->widthBoard = $widthBoard;
    }

    /**
     * @return array
     */
    public function getLivingCells()
    {
        return $this->livingCells;
    }

    /**
     * @param Cell $cell
     * @throws \Exception
     */
    public function insertCell($cell)
    {
        if ($cell->getPositionX() <= $this->getWidthBoard()
            && $cell->getPositionY() <= $this->getHeightBoard()
            && $cell->getPositionX() > 0 && $cell->getPositionY() > 0
        ) {
            $this->livingCells[] = $cell;
        }
    }

    public function getPotentialCells()
    {
        $potentials = [];

        $potentials = array_merge($potentials, $this->getLivingCells());

        /** @var Cell $livingCell */
        foreach ($this->getLivingCells() as $livingCell) {
            $neighbours = $livingCell->getNeighbours();
            $potentials = array_merge($potentials, $neighbours);
        }

        $potentials = array_map("unserialize", array_unique(array_map("serialize", $potentials)));

        return $potentials;
    }

    /**
     * @return Board
     */
    public function nextGeneration()
    {
        $nextGeneration = [];

        $potentials = $this->getPotentialCells();
        $livingCells = $this->getLivingCells();

        /** @var Cell $potentialCell */
        foreach ($potentials as $potentialCell) {
            if ($potentialCell->surviveIn($livingCells)) {
                $nextGeneration[] = $potentialCell;
            }
        }

        $nextBoard = new Board($this->getHeightBoard(), $this->getWidthBoard());

        /** @var Cell $cell */
        foreach ($nextGeneration as $cell) {
            $nextBoard->insertCell($cell);
        }

        return $nextBoard;
    }
}

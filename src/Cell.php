<?php
/**
 * Created by PhpStorm.
 * User: raul
 * Date: 9/07/18
 * Time: 21:37
 */

namespace GameOfLife;


class Cell
{
    private static $surviveWithValues = [2, 3];
    private static $comeAliveAgainWithValues = [3];

    /** @var  int */
    private $positionX;

    /** @var  int */
    private $positionY;

    /**
     * Cell constructor.
     * @param int $positionX
     * @param int $positionY
     */
    public function __construct($positionX, $positionY)
    {
        $this->positionX = $positionX;
        $this->positionY = $positionY;
    }

    /**
     * @return int
     */
    public function getPositionX(): int
    {
        return $this->positionX;
    }

    /**
     * @param int $positionX
     */
    public function setPositionX(int $positionX)
    {
        $this->positionX = $positionX;
    }

    /**
     * @return int
     */
    public function getPositionY(): int
    {
        return $this->positionY;
    }

    /**
     * @param int $positionY
     */
    public function setPositionY(int $positionY)
    {
        $this->positionY = $positionY;
    }

    /** @return array */
    public function getNeighbours()
    {
        return [
            new self($this->getPositionX() + 1, $this->getPositionY()),
            new self($this->getPositionX() + 1, $this->getPositionY() - 1),
            new self($this->getPositionX(), $this->getPositionY() - 1),
            new self($this->getPositionX() - 1, $this->getPositionY() - 1),
            new self($this->getPositionX() - 1, $this->getPositionY()),
            new self($this->getPositionX() - 1, $this->getPositionY() + 1),
            new self($this->getPositionX(), $this->getPositionY() + 1),
            new self($this->getPositionX() + 1, $this->getPositionY() + 1),
        ];
    }

    /**
     * @param Cell $neighbourCell
     * @return bool
     */
    public function isANeighbour($neighbourCell)
    {
        return in_array($neighbourCell, $this->getNeighbours());
    }

    /**
     * @param array $environment
     * @return int
     */
    public function numberOfNeighboursIn($environment)
    {
        $numberOfNeighbours = 0;

        /** @var Cell $possibleNeighborCell */
        foreach ($environment as $possibleNeighborCell) {
            if ($this->isANeighbour($possibleNeighborCell)) {
                $numberOfNeighbours++;
            }
        }

        return $numberOfNeighbours;
    }

    /**
     * @param array $environment
     * @return bool
     */
    public function surviveIn($environment)
    {
        $neighbours = $this->numberOfNeighboursIn($environment);

        if (in_array($this, $environment)) {
            return in_array($neighbours, self::$surviveWithValues);
        }

        return in_array($neighbours, self::$comeAliveAgainWithValues);
    }
}
